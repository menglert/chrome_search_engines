# Demo
* Type the **Keyword** (e.g. apex) for the App you want to search in the Chrome Browser
* Hit **Tab**
* Type your **Search Term**

![demo](./img/demo.gif)

# Install Search Engines
## MacOS / Linux
* Download [Zscaler Search Engines]
* **!Close the Browser (all windows) as the sqlite DB is locked otherwise!**
* Connect to the DB according to the Chrome distro you use
  * **!Replace `Default` if you use multiple Browser Profiles (e.g. with `Profile\ 1`)!**
  * **!The `sqlite3` command has to be executed from within the folder where you downloaded the [Zscaler Search Engines] file to!**
  * Chrome Browser - `sqlite3 ~/Library/Application\ Support/Google/Chrome/Default/Web\ Data`
  * Chromium Browser - `sqlite3 ~/Library/Application\ Support/Google/Chromium/Default/Web\ Data`
  * Chrome Canary Browser - `sqlite3 ~/Library/Application\ Support/Google/Chrome\ Canary/Default/Web\ Data`
  * Brave Browser - `sqlite3 ~/Library/Application\ Support/BraveSoftware/Brave-Browser/Default/Web\ Data`
* Import the **Search Egnines**
  * `.mode csv`
  * `.import zscaler_keywords.csv keywords`
  * `.exit`
* Verify new Search Engines
  * Open your Browser
  * Go to the **Search Engine Settings**
     * `chrome://settings/searchEngines`
  * Within **Other search engines** you should find the newly imported engines

# (Optional) Configure Additional Search Engines
* Go to the **Search Engine Settings**
  * `chrome://settings/searchEngines`
* Add a new one
* Configure **Name**, **Keyword** (watch out for duplicates) and **URL**

![configure](./img/configure.gif)

# Search Engines
| Name               | Keyword    | Additional Notes                    |
| ------------------ | ---------- | ----------------------------------- |
| Absorb             | train      |                                     |
| Apex               | apex       | `type:article` or `file_attachment` |
| Bitbucket          | bit        |                                     |
| Community          | com        |                                     |
| Confluence         | confluence |                                     |
| Engineering Wiki   | eng        |                                     |
| Google Cloudsearch | gsuite     |                                     |
| Help               | help       |                                     |
| IT HelpDesk        | it         |                                     |
| Jira               | jira       |                                     |
| Jobvite Candidates | candidate  |                                     |
| Jobvite Requ.      | req        |                                     |
| Klue               | klue       |                                     |
| Lucidchart         | lucid      |                                     |
| rfpio Answers      | rfpa       |                                     |
| rfpio Documents    | rfpd       |                                     |
| SaaS App Discovery | saas       |                                     |
| Salesforce         | sfdc       |                                     |
| Showpad            | show       |                                     |
| Wavefront          | wavefront  |                                     |
| Zendesk            | zendesk    |                                     |
| Zscaler.com        | zscaler    |                                     |

[Zscaler Search Engines]: ./zscaler_keywords.csv